from django.db import models
class dept(models.Model):
    deptname=models.CharField(max_length=20)
    def __str__(self):
        return self.deptname
class employees(models.Model):
    firstname=models.CharField(max_length=20)
    lastname=models.CharField(max_length=20)
    emp_id=models.IntegerField()
    emp_dept=models.ForeignKey(dept,on_delete=models.CASCADE,null=True)
    def __str__(self):
        return self.firstname

