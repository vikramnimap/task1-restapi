from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework .response import Response
from rest_framework import status
from .models import employees,dept
from .serializers import employeesSerializer

class employeeList(APIView):
    def get(self,request):
        employees1=employees.objects.all()
        serializer=employeesSerializer(employees1, many=True)
        return Response(serializer.data)
    def post(self,request):
        prd=request.data
        serializer= employeesSerializer(prd,many=True)
        if serializer.is_valid():
            serializer.save()
        return Response(request.data,status=201)

